package main

import "fmt"

func func1(n int) {
	switch {
	case 0 < n && n < 3:
		fmt.Println("0 < n < 3")
	case 3 < n && n < 6:
		fmt.Println("3 < n < 6")
	}
}

func StatementCase() {
	func1(4)
	func1(7)
}
