package main

import "fmt"

type t struct {
	Id   int
	Name string
}

func (tt *t) String() string {
	return fmt.Sprintf("[%d]: %s", tt.Id, tt.Name)
}

func Stringer() {
	t := &t{Id: 1, Name: "Hirate"}
	fmt.Println(t)
}
