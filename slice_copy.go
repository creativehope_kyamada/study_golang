package main

import "fmt"

func SliceCopy() {
	s1 := []int{1, 2, 3, 4, 5}
	s2 := []int{10, 11}

	s3 := copy(s1, s2)
	fmt.Printf("s1: %#v\n", s1)
	fmt.Printf("s2: %#v\n", s2)
	fmt.Printf("s3: %#v\n", s3)

	s4 := []int{1, 2}
	s5 := []int{10, 11, 12, 13, 14}

	s6 := copy(s4, s5)
	fmt.Printf("s4: %#v\n", s4)
	fmt.Printf("s5: %#v\n", s5)
	fmt.Printf("s6: %#v\n", s6)

	b := make([]byte, 9)
	n := copy(b, "あいうえお")
	fmt.Println("%#v, %#v\n", n, string(b))
}
