package main

func main() {
	SliceStruct()
	Tag()
	TagJson()
	Interface()
	Stringer()
	Function()
	Closure()
	StructStudy()
	TypeAliase()
	StatementSwitch()
	StatementCase()
	TypeAssertion()
	TypeSwitch()
	Label()
	Slice1()
	Slice2()
	SliceCopy()
	CompleteSlice()
}
