package main

import "fmt"

type (
	IntPair  [2]int
	Strings  []string
	AreaMap  map[string][2]float32
	IntsChan chan []int
)

type I0 int
type I1 int

func TypeAliase() {
	p := IntPair{1, 2}
	s := Strings{"hoge", "fuga", "foo", "bar"}
	a := AreaMap{
		"kyoto": {10.1, 10.2},
		"osaka": {20.1, 20.2},
	}
	c := make(IntsChan)

	fmt.Println(p)
	fmt.Println(s)
	fmt.Println(a)
	fmt.Println(c)

	i0 := I0(3)
	i1 := I1(2)

	i := 0
	//i = i0 <- cannot assign
	i = int(i0)

	i0 = 1
	i0 = I0(i1)

	fmt.Println(i)
	fmt.Println(i0)
	fmt.Println(i1)
}
