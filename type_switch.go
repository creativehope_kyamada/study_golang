package main

import "fmt"

func type_switch(x interface{}) {
	switch v := x.(type) {
	case bool:
		fmt.Println("bool: ", v)
	case int:
		fmt.Println(v * v)
	case string:
		fmt.Println(v)
	default:
		fmt.Printf("%#v\n", v)
	}
}

func TypeSwitch() {
	var i interface{} = false
	type_switch(i)

	i = 3
	type_switch(i)

	i = "hoge"
	type_switch(i)

	i = 2.0
	type_switch(i)

	i = [...]int{1, 2, 3, 4, 5}
	type_switch(i)
}
