package main

import "fmt"

func CountUp() func() int {
	i := 0
	return func() int {
		defer func() { i += 1 }()
		return i
	}
}

func Closure() {
	c1 := CountUp()
	c2 := CountUp()

	fmt.Println("c1: ", c1())
	fmt.Println("c2: ", c2())

	c1()
	c1()
	c1()
	fmt.Println("c1: ", c1())
	fmt.Println("c2: ", c2())
}
