package main

import (
	"encoding/json"
	"fmt"
)

type user struct {
	Id   int    `json:"user_id"`
	Name string `json:"user_name"`
	Age  int    `json:"user_age"`
}

func TagJson() {
	u := user{Id: 1, Name: "Taro", Age: 32}
	bs, _ := json.Marshal(u)
	fmt.Println(string(bs))
}
