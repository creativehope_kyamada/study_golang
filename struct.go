package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

type Feed struct {
	Name   string
	Amount int
}

type Animal struct {
	Name string
	Feed Feed
}

func StructStudy() {
	person := Person{"hoge", 20}
	people := []Person{
		{"hirate", 17},
		{"ikuta", 21},
	}
	fmt.Println(person)
	fmt.Println(people)

	animal := Animal{
		"Monkey",
		Feed{
			"Banana", 1,
		},
	}

	fmt.Println(animal)
}
