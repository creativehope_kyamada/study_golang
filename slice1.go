package main

import "fmt"

func Slice1() {
	s := make([]int, 5)
	a := [5]int{}

	fmt.Printf("%#v\n", s)
	fmt.Printf("%#v\n", a)

	s = make([]int, 5, 10)
	fmt.Println(len(s))
	fmt.Println(cap(s))

	//fmt.Println(s[5]) => runtime panic!
	fmt.Println(s[0:6])
	fmt.Println(s[0:10])
	//fmt.Println(s[0:11]) => runtime panic!

	fmt.Println("abced"[1:3])
	fmt.Println("あいうえお"[1:3])
	fmt.Println("あいうえお"[3:9])
}
