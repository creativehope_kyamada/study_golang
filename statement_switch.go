package main

import "fmt"

func StatementSwitch() {
	n := 3
	switch n {
	case 1:
		fmt.Println("one")
	case 2.0:
		fmt.Println("two")
	case 3 + 0i:
		fmt.Println("three")
	}
}
