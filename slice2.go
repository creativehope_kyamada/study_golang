package main

import "fmt"

func Slice2() {
	s0 := []int{1, 2, 3}
	s1 := []int{4, 5, 6}

	s2 := append(s0, s1...)
	fmt.Println(s2)

	s3 := [...]int{7, 8, 9}
	//s2 = append(s3, s1) compile error => s3はint配列 [...]int{}リテラルは配列のもの
	fmt.Println(s3)

	b := []byte{}
	b = append(b, "あいうえお"...)
	b = append(b, "かきくけこ"...)
	b = append(b, "さしすせそ"...)
	fmt.Println(string(b))
}
