package main

import "fmt"

type stringify interface {
	ToString() string
}

type person struct {
	Name string
	Age  int
}

func (p *person) ToString() string {
	return fmt.Sprintf("%s(%d)", p.Name, p.Age)
}

type car struct {
	Number string
	Model  string
}

func (c *car) ToString() string {
	return fmt.Sprintf("[%s] %s", c.Number, c.Model)
}

func Interface() {
	vs := []stringify{
		&person{Name: "Hirate", Age: 17},
		&car{Number: "1234", Model: "Fit"},
	}

	for _, v := range vs {
		fmt.Println(v.ToString())
	}
}
