package main

import "fmt"

func type_assertion(x interface{}) {
	if x == nil {
		fmt.Println("x is nil")
	} else if i, isInt := x.(int); isInt {
		fmt.Printf("x is integer: %d\n", i)
	} else if s, isString := x.(string); isString {
		fmt.Println("x is string: ", s)
	} else {
		fmt.Println("unsupported type!")
	}
}

func TypeAssertion() {
	var i interface{} = 3
	type_assertion(i)
	i = "hoge"
	type_assertion(i)
	i = 3.0
	type_assertion(i)
}
