package main

import "fmt"

type point struct {
	X, Y int
}
type points []*point

func (ps points) ToString() string {
	str := ""
	for _, p := range ps {
		if str != "" {
			str += ","
		}
		if p == nil {
			str += "<nil>"
		} else {
			str += fmt.Sprintf("[%d, %d]", p.X, p.Y)
		}
	}
	return str
}

func SliceStruct() {
	ps := points{}
	ps = append(ps, &point{X: 1, Y: 2})
	ps = append(ps, nil)
	ps = append(ps, &point{X: 3, Y: 4})
	fmt.Println(ps.ToString())
}
