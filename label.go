package main

import "fmt"

func Label() {
	for i := 0; i < 3; i++ {
	loop1:
		for j := 0; j < 3; j++ {
			for k := 0; k < 3; k++ {
				for l := 0; l < 3; l++ {
					if 1 < l {
						break loop1
					}
					fmt.Println("i, j, k, l = ", i, j, k, l)
				}
				fmt.Println("i, j, k = ", i, j, k)
			}
			fmt.Println("i, j = ", i, j)
		}
		fmt.Println("i = ", i)
	}

	for i := 0; i < 3; i++ {
	loop2:
		for j := 0; j < 3; j++ {
			for k := 0; k < 3; k++ {
				for l := 0; l < 3; l++ {
					if 1 < l {
						continue loop2
					}
					fmt.Println("i, j, k, l = ", i, j, k, l)
				}
				fmt.Println("i, j, k = ", i, j, k)
			}
			fmt.Println("i, j = ", i, j)
		}
		fmt.Println("i = ", i)
	}
}
