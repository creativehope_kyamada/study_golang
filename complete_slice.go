package main

import "fmt"

func CompleteSlice() {
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	b := a[:3]
	fmt.Printf("b: %#v, len: %v, cap: %v\n", b, len(b), cap(b))

	c := b[:10:10]
	fmt.Printf("c: %#v, len: %v, cap: %v\n", c, len(c), cap(c))
}
