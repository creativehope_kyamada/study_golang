package main

import "fmt"

func callFunc(f func(int) int, a int) {
	fmt.Println("callFunc: ", f(a))
}

func Function() {
	f := func(a int) int { return 2 * a }
	callFunc(f, 2)
}
